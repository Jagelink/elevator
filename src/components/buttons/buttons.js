import React from 'react';
import { connect } from 'react-redux';
import { elevator } from "../../redux/actions";
import "../../styling/buttonStyling.scss" ;

const Button = ({ value, elevator, name }) => {

    return (
        <div className="button">
            <div className="button-outer">
                <div className="button-inner">
                    <button
                        onClick={(e) => elevator(e)}
                        name={name}
                    >
                        {
                            value
                        }
                    </button>
                </div>
            </div>
        </div>
    );
};

export default connect(null, { elevator })(Button);
