import React, { useEffect } from 'react';
// Redux import
import { connect } from 'react-redux';
// Styling and Image
import '../../styling/liftStyling.scss';

const Elevator = ({ floor }) => {

    // useEffect replaces DidMount and DidUpdate
    useEffect(() => {
        // Selecting classes from liftStyling.scss to change in the switch statement
        const person = document.querySelector('.person');
        const hallway = document.querySelector('.hallway');
        switch (floor) {
            // Cases for changing styling when changing Storeys
            case 'BG':
                person.style.bottom = 80 + 'px';
                hallway.style.bottom = 80 + 'px';
                hallway.style.opacity = 1;
                hallway.style.backgroundImage = 'url(' + require('../../assets/svg/hallway_bg.svg') + ')';
                break;
            case '1':
                person.style.bottom = 160 + 'px';
                hallway.style.bottom = 160 + 'px';
                hallway.style.opacity = 1;
                hallway.style.backgroundImage = 'url(' + require('../../assets/svg/hallway_1.svg') + ')';
                break;
            case '2':
                person.style.bottom = 240 + 'px';
                hallway.style.bottom = 240 + 'px';
                hallway.style.opacity = 1;
                hallway.style.backgroundImage = 'url(' + require('../../assets/svg/hallway_2.svg') + ')';
                break;
            case '3':
                person.style.bottom = 320 + 'px';
                hallway.style.bottom = 320 + 'px';
                hallway.style.opacity = 1;
                hallway.style.backgroundImage = 'url(' + require('../../assets/svg/hallway_3.svg') + ')';
                break;
            case '4':
                person.style.bottom = 400 + 'px';
                hallway.style.bottom = 400 + 'px';
                hallway.style.opacity = 1;
                hallway.style.backgroundImage = 'url(' + require('../../assets/svg/hallway_4.svg') + ')';
                break;
            case '5':
                person.style.bottom = 480 + 'px';
                hallway.style.bottom = 480 + 'px';
                hallway.style.opacity = 1;
                hallway.style.backgroundImage = 'url(' + require('../../assets/svg/hallway_5.svg') + ')';
                break;
            case '6':
                person.style.bottom = 560 + 'px';
                hallway.style.bottom = 560 + 'px';
                hallway.style.opacity = 1;
                hallway.style.backgroundImage = 'url(' + require('../../assets/svg/hallway_6.svg') + ')';
                break;
            case '7':
                person.style.bottom = 640 + 'px';
                hallway.style.bottom = 640 + 'px';
                hallway.style.opacity = 1;
                hallway.style.backgroundImage = 'url(' + require('../../assets/svg/hallway_7.svg') + ')';
                break;
            case '8':
                person.style.bottom = 720 + 'px';
                hallway.style.bottom = 720 + 'px';
                hallway.style.opacity = 1;
                hallway.style.backgroundImage = 'url(' + require('../../assets/svg/hallway_8.svg') + ')';
                break;
            case '9':
                person.style.bottom = 800 + 'px';
                hallway.style.bottom = 800 + 'px';
                hallway.style.opacity = 1;
                hallway.style.backgroundImage = 'url(' + require('../../assets/svg/hallway_9.svg') + ')';
                break;
            default:
                person.style.bottom = 80 + 'px';
                hallway.style.bottom = 80 + 'px';
                hallway.style.opacity = 1;
                hallway.style.backgroundImage = 'url(' + require('../../assets/svg/hallway_bg.svg') + ')';
        }
    }, [floor]);

    // Return for styling when changing floors
    return (
        <div className={"hallway-container"}>
            <div className="person">{floor}</div>
            <div className={"hallway"}></div>
        </div>
    );
};
// Selecting data from the store.js
const mapStateToProps = (state) => ({
    floor: state.btnReducer.floor,
});
export default connect(mapStateToProps)(Elevator);
