import React from 'react';
import '../../styling/containerStyling.scss';
const Container = () => {

    return (
        <div className="container-outer">
            <div className="container-inner">
                <div className="container-main">
                </div>
            </div>
        </div>
    );
};
export default Container;
