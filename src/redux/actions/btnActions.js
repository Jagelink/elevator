// Imported type
import { MOVE } from './types';

// Calls the action and gives it to the right data value
export const elevator = (e) => dispatch => {
    const name = e.target.getAttribute('name');
    dispatch({ type: MOVE, name });
};
