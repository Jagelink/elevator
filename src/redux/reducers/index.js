import { combineReducers } from "redux";
import btnReducer from "./btnReducer";

const reducers = combineReducers({
    btnReducer,
})

export default reducers;
