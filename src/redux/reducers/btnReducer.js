// First value
const initialState = {
    floor: 'BG'
};

// Calls the state and returns correct data
const btnReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'MOVE':
            return {
                ...state,
                // attaching name to floor
                floor: action.name,
            };
        default:
            return state;
    }
};

export default btnReducer;
