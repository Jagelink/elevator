import { createStore, compose, applyMiddleware } from "redux";
// Redux-Thunk for making an action creator able to return a function
import thunk from "redux-thunk";
// importing the reducers
import reducers from './reducers';

const middleWare = [thunk];
const initialState = {};
export const store = createStore(
    reducers,
    initialState,
    compose(
        applyMiddleware(...middleWare)
    )
);
