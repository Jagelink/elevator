// Data for the buttons next to the floor (its inverted compared to the index.js file in this folder)
const floorButtonData = [
    {
        name: '9',
    },
    {
        name: '8',
    },
    {
        name: '7',
    },
    {
        name: '6',
    },
    {
        name: '5',
    },
    {
        name: '4',
    },
    {
        name: '3',
    },
    {
        name: '2',
    },
    {
        name: '1',
    },
    {
        name: 'BG',
    },
];

export default floorButtonData;
