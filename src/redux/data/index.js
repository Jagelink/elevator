// Data for the buttons in the elevator panel on the right side of the page
const elevatorData = [
    {
        name: '1',
    },
    {
        name: '2',
    },
    {
        name: '3',
    },
    {
        name: '4',
    },
    {
        name: '5',
    },
    {
        name: '6',
    },
    {
        name: '7',
    },
    {
        name: '8',
    },
    {
        name: '9',
    },
    {
        name: 'BG',
    },
]

export default elevatorData;
