import React from 'react';
// components
import Buttons from './components/buttons/buttons';
import Container from './components/containers/';
import Elevator from './components/elevator/';
// redux data
import elevatorData from "./redux/data";
import floorButtonData from "./redux/data/floorButtonData";
// styling
import './styling/generalStyling.scss';

function App() {
  return (
    <div className="App">
        <div className={"btns-container"}>
            <div className={"btn-holder"}>
                <div className={"btn-holder-inner"}>
                    <h2>Elevator Panel</h2>
                    {
                        // mapping the elevator data to the panel
                        elevatorData.map((elevator, idx) => (
                            <Buttons key={idx} name={elevator.name} value={elevator.name} />
                        ))
                    }
                </div>
            </div>
            <div className={"btn-holder2"}>
                <div className={"btn-holder-inner"}>
                    {
                        // mapping the button data to the buttons next to the elevator storeys
                        floorButtonData.map((switchButton, idx) => (
                            <Buttons key={idx} name={switchButton.name} />
                        ))
                    }
                </div>
            </div>
            <div className={"ctn-holder"}>
                {
                    // mapping the elevator itself so it moves
                    elevatorData.map((container, idx) => (
                        <Container key={idx} />
                    ))
                }
                <Elevator />
            </div>
        </div>
    </div>
  );
}

export default App;
