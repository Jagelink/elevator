<img src="./src/assets/JPG/elevator-preview.png" alt="">

# Elevator project Jasper Agelink

This elevator contains 2 ways of navigating. The panel on the left side of the screen and the buttons next to each storey.

### Installation

```
npm install
```

### Getting started

```
npm start
```
